import java.util.ArrayList;
import java.util.Collections;

public class Main {
    public static void main(String[] args) {
        int consumersCount = Integer.parseInt(args[0]);
        int producersCount = Integer.parseInt(args[1]);
        int maxSize = Integer.parseInt(args[2]);
        int itemsCount = Integer.parseInt(args[3]);

        StorageManager manager = new StorageManager(maxSize);
        ArrayList<Thread> allThreads = new ArrayList<>(consumersCount + producersCount);
        for (int i = 1; i <= consumersCount; i++) {
            allThreads.add(
                    new Thread(
                            new Consumer(
                                    manager,
                                    i,
                                    itemsCount / consumersCount + ((i == consumersCount) ? itemsCount % consumersCount : 0))));
        }
        for (int i = 1; i <= producersCount; i++) {
            allThreads.add(
                    new Thread(
                            new Producer(
                                    manager,
                                    i,
                                    itemsCount / producersCount + ((i == producersCount) ? itemsCount % producersCount : 0))));
        }
        Collections.shuffle(allThreads);
        allThreads.forEach(Thread::start);
    }
}