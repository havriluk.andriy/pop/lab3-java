import java.util.LinkedList;
import java.util.concurrent.Semaphore;

public class StorageManager {

    private Semaphore empty;
    private Semaphore full;
    private Semaphore used;
    private LinkedList<String> items = new LinkedList<>();

    public StorageManager(int maxSize) {
        empty = new Semaphore(0);
        full = new Semaphore(maxSize);
        used = new Semaphore(1);
    }

    public void addItem(String item) throws InterruptedException {
        full.acquire();
        used.acquire();
        items.add(item);
        System.out.println("Додано новий предмет: " + item);
        empty.release();
        used.release();
    }

    public void getItem(int consumer) throws InterruptedException {
        empty.acquire();
        used.acquire();
        String item = items.removeFirst();
        System.out.println("Споживач " + consumer + " вилучив предмет: " + item);
        full.release();
        used.release();
    }
}
