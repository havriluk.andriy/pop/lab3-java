public class Consumer implements Runnable {
    private StorageManager manager;
    private int id;
    private int itemsToConsume;

    public Consumer(StorageManager manager, int id, int itemsToConsume) {
        this.manager = manager;
        this.id = id;
        this.itemsToConsume = itemsToConsume;
    }

    @Override
    public void run() {
        try {
            for (int i = 1; i <= itemsToConsume; i++) {
                manager.getItem(id);
            }
        } catch (InterruptedException e) {
            System.out.println("Interrupted Exception");
            e.printStackTrace();
        }
    }
}
