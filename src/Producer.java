public class Producer implements Runnable {

    private StorageManager manager;
    private int id;
    private int itemsToProduce;

    public Producer(StorageManager manager, int id, int itemsToProduce) {
        this.manager = manager;
        this.id = id;
        this.itemsToProduce = itemsToProduce;
    }

    @Override
    public void run() {
        try {
            for (int i = 1; i <= itemsToProduce; i++) {
                manager.addItem(id + "#" + i);
            }
        } catch (InterruptedException e) {
            System.out.println("Interrupted Exception!");
            e.printStackTrace();
        }
    }
}
